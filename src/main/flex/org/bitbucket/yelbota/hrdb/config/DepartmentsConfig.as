package org.bitbucket.yelbota.hrdb.config {
import org.bitbucket.yelbota.hrdb.application.departments.*;
import org.bitbucket.yelbota.hrdb.application.departments.create.CreateDepartmentMediator;
import org.bitbucket.yelbota.hrdb.application.departments.create.ICreateDepartmentView;

import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class DepartmentsConfig implements IConfig {

    [Inject]
    public var mediatorMap:IMediatorMap;

    public function configure():void {
        mediatorMap.map(IDepartmentsView).toMediator(DepartmentsViewMediator);
        mediatorMap.map(ICreateDepartmentView).toMediator(CreateDepartmentMediator);
    }
}

}
