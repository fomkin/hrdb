package org.bitbucket.yelbota.hrdb.config {
import org.bitbucket.yelbota.hrdb.application.popups.*;

import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IInjector;

/**
 * Employees model
 */
public class PopupsConfig implements IConfig {

    [Inject]
    public var injector:IInjector;

    public function configure():void {
        injector.map(IPopups).toSingleton(Popups);
    }
}

}
