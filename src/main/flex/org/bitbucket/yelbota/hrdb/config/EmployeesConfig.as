package org.bitbucket.yelbota.hrdb.config {

import org.bitbucket.yelbota.hrdb.application.employees.EmployeesViewMediator;
import org.bitbucket.yelbota.hrdb.application.employees.IEmployeesView;
import org.bitbucket.yelbota.hrdb.application.employees.take.ITakeEmployeeView;
import org.bitbucket.yelbota.hrdb.application.employees.take.TakeEmployeeViewMediator;

import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class EmployeesConfig implements IConfig {

    [Inject]
    public var mediatorMap:IMediatorMap;

    public function configure():void {
        mediatorMap.map(IEmployeesView).toMediator(EmployeesViewMediator);
        mediatorMap.map(ITakeEmployeeView).toMediator(TakeEmployeeViewMediator);
    }
}
}
