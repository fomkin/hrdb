package org.bitbucket.yelbota.hrdb.config {

import org.bitbucket.yelbota.hrdb.application.settings.ISettingsView;
import org.bitbucket.yelbota.hrdb.application.settings.SettingsViewMediator;

import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SettingsConfig implements IConfig {

    [Inject]
    public var mediatorMap:IMediatorMap;

    public function configure():void {
        mediatorMap.map(ISettingsView).toMediator(SettingsViewMediator);
    }
}
}
