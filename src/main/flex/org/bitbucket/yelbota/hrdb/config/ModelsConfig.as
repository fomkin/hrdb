package org.bitbucket.yelbota.hrdb.config {
import org.bitbucket.yelbota.hrdb.model.*;

import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IInjector;

public class ModelsConfig implements IConfig {

    [Inject]
    public var injector:IInjector;

    public function configure():void {
        injector.map(ISQLService).toSingleton(SQLService);
        injector.map(IDepartmentsModel).toSingleton(SQLDepartmentsModel);
        injector.map(IEmployeesModel).toSingleton(SQLEmployeesModel);
        injector.map(ISettingsModel).toSingleton(SharedObjectSettingsModel);
    }
}

}
