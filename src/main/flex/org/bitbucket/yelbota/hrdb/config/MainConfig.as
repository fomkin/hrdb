package org.bitbucket.yelbota.hrdb.config {
import org.bitbucket.yelbota.hrdb.application.main.*;

import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class MainConfig implements IConfig {

    [Inject]
    public var mediatorMap:IMediatorMap;

    public function configure():void {
        mediatorMap.map(IMainView).toMediator(MainViewMediator);
    }
}
}
