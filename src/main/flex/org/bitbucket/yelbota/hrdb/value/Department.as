package org.bitbucket.yelbota.hrdb.value {

/**
 * Department value object
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class Department extends ValueObjectBase {

    //-------------------------------------------------------------------------
    //
    //  Properties
    //
    //-------------------------------------------------------------------------

    [Bindable]

    /**
     * Name of department
     */
    public var name:String;

    [Bindable]

    /**
     * Number of employees in department.
     * NOTE that it's valid on the moment. Don't use
     * this property to make any calculation.
     */
    public var employeesNumber:uint;

    //-------------------------------------------------------------------------
    //
    //  Constructor
    //
    //-------------------------------------------------------------------------

    public function Department(name:String, employeesNumber:uint = 0, id:String = null) {
        super(id);
        this.name = name;
        this.employeesNumber = employeesNumber;
    }
}

}
