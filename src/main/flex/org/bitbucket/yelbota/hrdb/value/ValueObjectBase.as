package org.bitbucket.yelbota.hrdb.value {
import flash.events.EventDispatcher;

/**
 * Base value object class
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class ValueObjectBase extends EventDispatcher {

    [Bindable]
    public var id:String;

    public function ValueObjectBase(id:String) {
        this.id = id;
    }
}

}
