package org.bitbucket.yelbota.hrdb.value {

/**
 * Employee value object
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class Employee extends ValueObjectBase {

    //-------------------------------------------------------------------------
    //
    //  Properties
    //
    //-------------------------------------------------------------------------

    [Bindable]
    public var firstName:String;

    [Bindable]
    public var lastName:String;

    [Bindable]
    public var position:String;

    [Bindable]
    public var department:Department;

    //-------------------------------------------------------------------------
    //
    //  Constructor
    //
    //-------------------------------------------------------------------------

    public function Employee(firstName:String, secondName:String, position:String, department:Department, id:String = null) {
        super(id);
        this.firstName = firstName;
        this.lastName = secondName;
        this.position = position;
        this.department = department;
    }
}

}
