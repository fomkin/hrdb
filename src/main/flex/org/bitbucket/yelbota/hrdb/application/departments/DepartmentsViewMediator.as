package org.bitbucket.yelbota.hrdb.application.departments {

import mx.collections.ArrayList;

import org.bitbucket.yelbota.hrdb.application.popups.IPopups;
import org.bitbucket.yelbota.hrdb.model.IDepartmentsModel;
import org.bitbucket.yelbota.hrdb.model.errors.DepartmentIsNotEmptyError;
import org.bitbucket.yelbota.hrdb.value.Department;

import robotlegs.bender.bundles.mvcs.Mediator;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class DepartmentsViewMediator extends Mediator {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var popups:IPopups;

    [Inject]
    public var view:IDepartmentsView;

    [Inject]
    public var model:IDepartmentsModel;

    //-------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //-------------------------------------------------------------------------

    override public function destroy():void {
        super.destroy();
        view.dispose();
        model = null;
        view = null;
    }

    override public function initialize():void {
        updateDepartmentList();

        model.onUpdate.add(updateDepartmentList);

        view.onRefreshClick.add(handleRefreshClick);
        view.onDepartmentAddClick.add(handleDepartmentAddClick);
        view.onDepartmentRemoveClick.add(handleDepartmentRemoveClick);
    }

    //-------------------------------------------------------------------------
    //
    //  Private methods
    //
    //-------------------------------------------------------------------------

    private function updateDepartmentList():void {
        model.getAllDepartmentsWithNumEmployees().subscribe(
                model_getAllDepartmentsWithNumEmployeesResult
        );
    }

    //-------------------------------------------------------------------------
    //
    //  Event handlers
    //
    //-------------------------------------------------------------------------

    private function handleRefreshClick():void {
        updateDepartmentList();
    }

    private function handleDepartmentRemoveClick():void {
        view.disableRemoveButton();
        model.removeDepartment(view.getSelectedDepartment()).subscribe(
                function (ok:Boolean):void {
                    view.enableRemoveButton();
                },
                function (error:Error):void {
                    view.enableRemoveButton();
                    if (error is DepartmentIsNotEmptyError) {
                        popups.alert("This department is not empty. Fire all employees\n"
                                + "or transfer them to another department");
                    }
                    else {
                        throw error;
                    }
                }
        );
    }

    private function handleDepartmentAddClick():void {
        popups.createDepartmentWindow();
    }

    private function model_getAllDepartmentsWithNumEmployeesResult(result:Vector.<Department>):void {
        var collection:ArrayList = new ArrayList();
        for each (var department:Department in result) {
            collection.addItem(department);
        }
        view.setDepartments(collection);
    }

}

}
