package org.bitbucket.yelbota.hrdb.application.employees.take {

import mx.collections.IList;

import org.bitbucket.yelbota.hrdb.application.popups.IClosable;

import org.bitbucket.yelbota.hrdb.value.Employee;
import org.osflash.signals.Signal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface ITakeEmployeeView extends IClosable {

    function get value():Employee;

    function get onTakeClick():Signal;

    function setDepartments(collection:IList):void;

    function dispose():void;
}
}
