package org.bitbucket.yelbota.hrdb.application.main {
import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IMainView {

    function activate():void;

    function showProgress():void;

    function hideProgress():void;

    function get onSettingsClick():ISignal;
}

}
