package org.bitbucket.yelbota.hrdb.application.employees {

import mx.collections.IList;

import org.bitbucket.yelbota.hrdb.value.Department;

import org.bitbucket.yelbota.hrdb.value.Employee;
import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IEmployeesView {

    function get selectorValues():Employee;

    function get onSelect():ISignal;

    function get onTake():ISignal;

    function get onFireAll():ISignal;

    function get onFireHighlighted():ISignal;

    function get onTransferAll():ISignal;

    function get onTransferHighlighted():ISignal;

    function get selectedDepartmentToTransfer():Department;

    function get highlightedEmployee():Employee;

    function get onDepartmentToTransferSelected():ISignal;

    function setDepartmentsForSelector(collection:IList, selectedItem:Department):void;

    function setEmployees(collection:IList):void;

    function enableActionsWithSelection():void;

    function showTransferEmployeesWindow(departments:IList):void;
}

}
