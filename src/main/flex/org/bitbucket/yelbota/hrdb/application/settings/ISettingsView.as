package org.bitbucket.yelbota.hrdb.application.settings {

import org.bitbucket.yelbota.hrdb.application.popups.IClosable;
import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface ISettingsView extends IClosable {

    function get onDatabasePathSelected():ISignal;

    function get selectedDatabasePath():String;

    function set selectedDatabasePath(value:String):void;

    function dispose():void;
}
}
