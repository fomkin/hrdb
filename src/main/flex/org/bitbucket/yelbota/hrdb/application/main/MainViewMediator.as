package org.bitbucket.yelbota.hrdb.application.main {

import org.bitbucket.yelbota.hrdb.application.popups.IPopups;
import org.bitbucket.yelbota.hrdb.model.ISQLService;
import org.bitbucket.yelbota.hrdb.model.ISettingsModel;

import robotlegs.bender.bundles.mvcs.Mediator;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class MainViewMediator extends Mediator {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var view:IMainView;

    [Inject]
    public var sqlService:ISQLService;

    [Inject]
    public var settingsModel:ISettingsModel;

    [Inject]
    public var popups:IPopups;

    //-------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //-------------------------------------------------------------------------

    override public function initialize():void {

        view.onSettingsClick.add(view_handleSettingsClick);

        sqlService.onConnect.add(handleConnect);
        sqlService.onStartProcessQuery.add(handleStartProcessQuery);
        sqlService.onFinishProcessQuery.add(handleFinishProcessQuery);

        if (settingsModel.database == null || !settingsModel.database.exists) {
            popups.settings();
        }
        else {
            sqlService.open(settingsModel.database);
        }
    }

    //-------------------------------------------------------------------------
    //
    //  Event Handlers
    //
    //-------------------------------------------------------------------------

    private function view_handleSettingsClick():void {
        popups.settings();
    }

    private function handleFinishProcessQuery():void {
        view.hideProgress();
    }

    private function handleStartProcessQuery():void {
        view.showProgress();
    }

    private function handleConnect():void {
        view.activate();
    }
}

}
