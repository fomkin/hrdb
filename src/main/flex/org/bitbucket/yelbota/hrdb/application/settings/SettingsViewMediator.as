package org.bitbucket.yelbota.hrdb.application.settings {

import flash.filesystem.File;

import org.bitbucket.yelbota.hrdb.model.ISQLService;

import org.bitbucket.yelbota.hrdb.model.ISettingsModel;

import robotlegs.bender.bundles.mvcs.Mediator;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SettingsViewMediator extends Mediator {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var view:ISettingsView;

    [Inject]
    public var model:ISettingsModel;

    [Inject]
    public var sqlService:ISQLService;

    //-------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //-------------------------------------------------------------------------

    override public function destroy():void {
        super.destroy();
        view.dispose();
        view = null;
        model = null;
    }

    override public function initialize():void {
        super.initialize();
        view.onDatabasePathSelected.add(view_handleDatabaseSelected);
        view.selectedDatabasePath = model.database && model.database.exists
                ? model.database.nativePath
                : "<Not selected>";
    }

    //-------------------------------------------------------------------------
    //
    //  Event handlers
    //
    //-------------------------------------------------------------------------

    private function view_handleDatabaseSelected():void {
        var file:File = File.userDirectory.resolvePath(view.selectedDatabasePath);
        model.database = file;
        sqlService.open(file);
        view.close();
    }
}

}
