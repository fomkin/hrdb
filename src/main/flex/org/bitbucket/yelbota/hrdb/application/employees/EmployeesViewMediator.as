package org.bitbucket.yelbota.hrdb.application.employees {

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.utils.StringUtil;

import org.bitbucket.yelbota.hrdb.application.popups.IPopups;
import org.bitbucket.yelbota.hrdb.model.IDepartmentsModel;
import org.bitbucket.yelbota.hrdb.model.IEmployeesModel;
import org.bitbucket.yelbota.hrdb.model.supportClasses.EmployeeSelectionCriteria;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.bitbucket.yelbota.hrdb.value.Employee;

import robotlegs.bender.bundles.mvcs.Mediator;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class EmployeesViewMediator extends Mediator {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var view:IEmployeesView;

    [Inject]
    public var employeesModel:IEmployeesModel;

    [Inject]
    public var departmentsModel:IDepartmentsModel;

    [Inject]
    public var popups:IPopups;

    //-------------------------------------------------------------------------
    //
    //  Variables
    //
    //-------------------------------------------------------------------------

    private var doesNotMatterDepartment:Department = new Department("Doesn't mater");

    private var lastSelectionCriteria:Vector.<EmployeeSelectionCriteria>;

    /**
     * If true transfer all, if false transfer highlighted.
     */
    private var transferAllAfterDeptSelection:Boolean;

    //-------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //-------------------------------------------------------------------------

    override public function initialize():void {

        super.initialize();

        updateDepartmentsSelector();

        view.onSelect.add(view_handleSelect);
        view.onTake.add(view_handleTake);
        view.onFireHighlighted.add(view_handleFireHighlighted);
        view.onTransferHighlighted.add(view_handleTransferHighlighted);
        view.onDepartmentToTransferSelected.add(view_handleDepartmentToTransferSelected);
        view.onTransferAll.add(view_handleTransferAll);
        view.onFireAll.add(view_handleFireAll);

        departmentsModel.onUpdate.add(updateDepartmentsSelector);
    }

    override public function destroy():void {
        super.destroy();
    }

    //-------------------------------------------------------------------------
    //
    //  Private methods
    //
    //-------------------------------------------------------------------------

    private function updateDepartmentsSelector():void {
        departmentsModel
                .getAllDepartments()
                .subscribe(departmentsModel_getAllDepartmentsResult);
    }

    private function getCriteriaFromSelectorValues():Vector.<EmployeeSelectionCriteria> {

        var criteria:Vector.<EmployeeSelectionCriteria> = new Vector.<EmployeeSelectionCriteria>();
        var selectorValues:Employee = view.selectorValues;

        if (StringUtil.trim(selectorValues.firstName) != "") {
            criteria.push(new EmployeeSelectionCriteria(
                    EmployeeSelectionCriteria.FIRST_NAME,
                    selectorValues.firstName
            ));
        }

        if (StringUtil.trim(selectorValues.lastName) != "") {
            criteria.push(new EmployeeSelectionCriteria(
                    EmployeeSelectionCriteria.LAST_NAME,
                    selectorValues.lastName
            ));
        }

        if (StringUtil.trim(selectorValues.position) != "") {
            criteria.push(new EmployeeSelectionCriteria(
                    EmployeeSelectionCriteria.POSITION,
                    selectorValues.position
            ));
        }

        if (selectorValues.department != null && selectorValues.department != doesNotMatterDepartment) {
            criteria.push(new EmployeeSelectionCriteria(
                    EmployeeSelectionCriteria.DEPARTMENT,
                    selectorValues.department
            ));
        }

        return criteria;
    }


    //-------------------------------------------------------------------------
    //
    //  Event handlers
    //
    //-------------------------------------------------------------------------

    private function view_handleTake():void {
        popups.takeEmployeeWindow();
    }

    private function view_handleDepartmentToTransferSelected():void {
        if (transferAllAfterDeptSelection) {
            employeesModel.transferEmployeeByCriteria(lastSelectionCriteria, view.selectedDepartmentToTransfer)
                    .subscribe(employeesModel_basicQueryResult);
        }
        else {
            if (view.highlightedEmployee) {
                var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
                    new EmployeeSelectionCriteria(EmployeeSelectionCriteria.ID, view.highlightedEmployee.id)
                ];
                employeesModel.transferEmployeeByCriteria(criteria, view.selectedDepartmentToTransfer)
                        .subscribe(employeesModel_basicQueryResult);
            }
        }
    }

    private function view_handleFireAll():void {
        employeesModel.fireEmployeeByCriteria(lastSelectionCriteria).subscribe(employeesModel_basicQueryResult);
    }

    private function view_handleFireHighlighted():void {
        if (view.highlightedEmployee) {
            var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
                new EmployeeSelectionCriteria(EmployeeSelectionCriteria.ID, view.highlightedEmployee.id)
            ];
            employeesModel.fireEmployeeByCriteria(criteria).subscribe(employeesModel_basicQueryResult);
        }
    }

    private function view_handleTransferAll():void {
        departmentsModel.getAllDepartments().subscribe(departmentsModel_getAllDepartmentsResult2);
        transferAllAfterDeptSelection = true;
    }

    private function view_handleTransferHighlighted():void {
        departmentsModel.getAllDepartments().subscribe(departmentsModel_getAllDepartmentsResult2);
        transferAllAfterDeptSelection = false;
    }

    private function departmentsModel_getAllDepartmentsResult2(result:Vector.<Department>):void {
        var collection:ArrayList = new ArrayList();
        for each (var department:Department in result) {
            collection.addItem(department);
        }
        view.showTransferEmployeesWindow(collection);
    }

    private function departmentsModel_getAllDepartmentsResult(result:Vector.<Department>):void {
        var collection:ArrayList = new ArrayList();
        collection.addItem(doesNotMatterDepartment);
        for each (var department:Department in result) {
            collection.addItem(department);
        }
        view.setDepartmentsForSelector(collection, doesNotMatterDepartment);
    }

    private function view_handleSelect():void {
        var criteria:Vector.<EmployeeSelectionCriteria> = getCriteriaFromSelectorValues();
        if (criteria.length > 0) {
            employeesModel.selectEmployees(criteria).subscribe(employeesModel_selectedEmployeesResult);
            lastSelectionCriteria = criteria;
        }
        else {
            popups.alert("Can't make selection without criteria");
        }
    }

    private function employeesModel_basicQueryResult(ok:Boolean):void {
        if (ok && lastSelectionCriteria) {
            employeesModel.selectEmployees(lastSelectionCriteria).subscribe(employeesModel_selectedEmployeesResult);
        }
    }

    private function employeesModel_selectedEmployeesResult(result:Vector.<Employee>):void {
        var collection:ArrayCollection = new ArrayCollection();
        collection.disableAutoUpdate();
        for each (var employee:Employee in result) {
            collection.addItem(employee);
        }
        collection.enableAutoUpdate();
        view.enableActionsWithSelection();
        view.setEmployees(collection);
    }
}

}
