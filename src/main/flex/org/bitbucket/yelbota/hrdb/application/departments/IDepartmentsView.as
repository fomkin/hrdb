package org.bitbucket.yelbota.hrdb.application.departments {
import mx.collections.IList;

import org.bitbucket.yelbota.hrdb.value.Department;
import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IDepartmentsView {

    function get onDepartmentAddClick():ISignal;

    function get onDepartmentRemoveClick():ISignal;

    function get onRefreshClick():ISignal;

    function setDepartments(list:IList):void;

    function getSelectedDepartment():Department;

    function dispose():void;

    function disableRemoveButton():void;

    function enableRemoveButton():void;
}
}
