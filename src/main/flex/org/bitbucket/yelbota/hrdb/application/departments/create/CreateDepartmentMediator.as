package org.bitbucket.yelbota.hrdb.application.departments.create {

import org.bitbucket.yelbota.hrdb.application.popups.IPopups;
import org.bitbucket.yelbota.hrdb.model.IDepartmentsModel;
import org.bitbucket.yelbota.hrdb.value.Department;

import robotlegs.bender.bundles.mvcs.Mediator;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class CreateDepartmentMediator extends Mediator {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var view:ICreateDepartmentView;

    [Inject]
    public var model:IDepartmentsModel;

    [Inject]
    public var popups:IPopups;

    //-------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //-------------------------------------------------------------------------

    override public function initialize():void {
        view.onCreateDepartmentClick.add(handleCreateDepartmentClick);
    }

    override public function destroy():void {
        super.destroy();
        view.dispose();
        model = null;
    }

    //-------------------------------------------------------------------------
    //
    //  Event handlers
    //
    //-------------------------------------------------------------------------

    private function handleCreateDepartmentClick():void {
        view.enabled = false
        model.createDepartment(new Department(view.inputValue)).subscribe(
                function(ok:Boolean):void {
                    view.close();
                },
                function(error:Error):void {
                    popups.alert(error.message);
                    view.enabled = true;
                }
        );
    }
}

}
