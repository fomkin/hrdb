package org.bitbucket.yelbota.hrdb.application.popups {

import flash.events.IEventDispatcher;

import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IClosable extends IEventDispatcher {

    function close():void;

    function get onClose():ISignal;
}

}
