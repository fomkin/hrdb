package org.bitbucket.yelbota.hrdb.application.popups {

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IPopups {

    function alert(message:String):void;

    function createDepartmentWindow():void;

    function takeEmployeeWindow():void;

    function settings():void;
}

}
