package org.bitbucket.yelbota.hrdb.application.employees.take {

import mx.collections.ArrayList;

import org.bitbucket.yelbota.hrdb.model.IDepartmentsModel;
import org.bitbucket.yelbota.hrdb.model.IEmployeesModel;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.bitbucket.yelbota.hrdb.value.Employee;

import robotlegs.bender.bundles.mvcs.Mediator;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class TakeEmployeeViewMediator extends Mediator {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var view:ITakeEmployeeView;

    [Inject]
    public var departmentsModel:IDepartmentsModel;

    [Inject]
    public var employeesModel:IEmployeesModel;

    //-------------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //-------------------------------------------------------------------------

    override public function initialize():void {
        super.initialize();
        departmentsModel.getAllDepartments().subscribe(departmentsModel_getAllDepartmentsResult);
        view.onTakeClick.add(view_handleTakeClick);
    }

    override public function destroy():void {
        super.destroy();
        view.dispose();
        view = null;
        departmentsModel = null;
        employeesModel = null;
    }

    //-------------------------------------------------------------------------
    //
    //  Event handlers
    //
    //-------------------------------------------------------------------------

    private function view_handleTakeClick():void {
        var value:Employee = view.value;
        employeesModel.takeEmployee(value).subscribe(employeesModel_takeEmployeeResult);
    }

    private function employeesModel_takeEmployeeResult(ok:Boolean):void {
        if (ok) {
            view.close();
        }
    }

    private function departmentsModel_getAllDepartmentsResult(result:Vector.<Department>):void {
        var collection:ArrayList = new ArrayList();
        for each (var department:Department in result) {
            collection.addItem(department);
        }
        view.setDepartments(collection);
    }
}

}
