package org.bitbucket.yelbota.hrdb.application.departments.create {
import org.bitbucket.yelbota.hrdb.application.popups.IClosable;
import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface ICreateDepartmentView extends IClosable {

    function get enabled():Boolean;

    function set enabled(value:Boolean):void;

    function get onCreateDepartmentClick():ISignal;

    function get inputValue():String;

    function dispose():void;

}

}
