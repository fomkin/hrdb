package org.bitbucket.yelbota.hrdb.application.popups {

import flash.display.DisplayObjectContainer;
import flash.events.Event;

import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.managers.PopUpManager;

import org.bitbucket.yelbota.hrdb.application.departments.create.CreateDepartmentView;
import org.bitbucket.yelbota.hrdb.application.employees.take.TakeEmployeeView;
import org.bitbucket.yelbota.hrdb.application.settings.SettingsView;

import robotlegs.bender.extensions.viewManager.api.IViewManager;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class Popups implements IPopups {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var viewManager: IViewManager;

    //-------------------------------------------------------------------------
    //
    //  IPopups
    //
    //-------------------------------------------------------------------------

    public function alert(message:String):void {
        Alert.show(message, "Alert");
    }

    public function createDepartmentWindow():void {
        var addDepartmentWindow:CreateDepartmentView = new CreateDepartmentView();
        openWindow(addDepartmentWindow, addDepartmentWindow);
    }

    public function takeEmployeeWindow():void {
        var takeEmployeeView:TakeEmployeeView = new TakeEmployeeView();
        openWindow(takeEmployeeView, takeEmployeeView);
    }

    public function settings():void {
        var settingsView:SettingsView = new SettingsView();
        openWindow(settingsView, settingsView);
    }

    //-------------------------------------------------------------------------
    //
    //  Private methods
    //
    //-------------------------------------------------------------------------

    private function openWindow(closable:IClosable, component:UIComponent):void {

        function remove(e:Event = null):void {
            PopUpManager.removePopUp(component);
            viewManager.removeContainer(component);
        }

        viewManager.addContainer(component);
        closable.onClose.add(remove);
        closable.addEventListener(CloseEvent.CLOSE, remove);

        PopUpManager.addPopUp(component, FlexGlobals.topLevelApplication as DisplayObjectContainer, true);
        PopUpManager.centerPopUp(component);
    }

}

}
