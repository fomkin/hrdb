package org.bitbucket.yelbota.hrdb {

import org.bitbucket.yelbota.hrdb.config.DepartmentsConfig;
import org.bitbucket.yelbota.hrdb.config.EmployeesConfig;
import org.bitbucket.yelbota.hrdb.config.ModelsConfig;
import org.bitbucket.yelbota.hrdb.config.MainConfig;
import org.bitbucket.yelbota.hrdb.config.PopupsConfig;
import org.bitbucket.yelbota.hrdb.config.SettingsConfig;

import robotlegs.bender.bundles.mvcs.MVCSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.impl.Context;

import spark.components.WindowedApplication;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class HrdbApplication extends WindowedApplication {

    protected var context:IContext;

    public function HrdbApplication() {
        super();
        setupContext();
    }

    /**
     * Sets up the Robotlegs context.
     */
    private function setupContext():void {

        context = new Context()
                .install(MVCSBundle)
                .configure(ModelsConfig)
                .configure(SettingsConfig)
                .configure(PopupsConfig)
                .configure(DepartmentsConfig)
                .configure(MainConfig)
                .configure(EmployeesConfig)
                .configure(new ContextView(this));
    }

}

}
