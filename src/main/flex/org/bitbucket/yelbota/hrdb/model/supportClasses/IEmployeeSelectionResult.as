package org.bitbucket.yelbota.hrdb.model.supportClasses {

/**
 * Employee selection async result.
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IEmployeeSelectionResult extends IQueryResult {

    /**
     * @param resultHandler function (result:Vector.<Employee>)void { ... }
     * @param errorHandler function (error:String)void { ... }
     */
}

}
