package org.bitbucket.yelbota.hrdb.model.supportClasses {

/**
 * Employees model
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class EmployeeSelectionCriteria {

    //-------------------------------------------------------------------------
    //
    //  Types
    //
    //-------------------------------------------------------------------------

    public static const FIRST_NAME:String = "firstName";
    public static const LAST_NAME:String = "secondName";
    public static const POSITION:String = "position";
    public static const DEPARTMENT:String = "department";
    public static const ID:String = "id";

    //-------------------------------------------------------------------------
    //
    //  Properties
    //
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    //  type
    //-------------------------------------------------------------------------

    private var _type:String;

    public function get type():String {
        return _type;
    }

    //-------------------------------------------------------------------------
    //  value
    //-------------------------------------------------------------------------

    private var _value:*;

    public function get value():* {
        return _value;
    }

    //-------------------------------------------------------------------------
    //
    //  Constructor
    //
    //-------------------------------------------------------------------------

    public function EmployeeSelectionCriteria(type:String, value:*) {
        _type = type;
        _value = value;
    }
}

}
