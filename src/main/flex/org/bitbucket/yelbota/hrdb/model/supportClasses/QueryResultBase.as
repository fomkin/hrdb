package org.bitbucket.yelbota.hrdb.model.supportClasses {

import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class QueryResultBase {

    //-------------------------------------------------------------------------
    //
    //  Variables
    //
    //-------------------------------------------------------------------------

    private var subscribers:Vector.<Function>;
    private var errorHandlers:Vector.<Function>;
    private var statement:SQLStatement;

    //-------------------------------------------------------------------------
    //
    //  Public methods
    //
    //-------------------------------------------------------------------------

    public function QueryResultBase(statement:SQLStatement) {

        this.subscribers = new Vector.<Function>();
        this.errorHandlers = new Vector.<Function>();
        this.statement = statement;

        statement.addEventListener(SQLEvent.RESULT, statement_resultHandler);
        statement.addEventListener(SQLErrorEvent.ERROR, statement_errorHandler);
    }

    public function subscribe(resultHandler:Function, errorHandler:Function = null):void {
        subscribers.push(resultHandler);
        errorHandlers.push(errorHandler);
    }

    public function fail(error:Error):void {
        statement.removeEventListener(SQLEvent.RESULT, statement_resultHandler);
        statement.removeEventListener(SQLErrorEvent.ERROR, statement_errorHandler);
        broadcastError(error);
    }

    //-------------------------------------------------------------------------
    //
    //  Private methods
    //
    //-------------------------------------------------------------------------

    private function broadcastError(error:Error):void {
        for each (var callback:Function in errorHandlers) {
            if (callback != null)
                callback(error);
        }
    }

    //-------------------------------------------------------------------------
    //
    //  Protected methods
    //
    //-------------------------------------------------------------------------

    protected function parseResult(result:SQLResult):* {
        throw new Error("Not implemented");
    }

    protected function statement_errorHandler(event:SQLErrorEvent):void {
        broadcastError(event.error);
    }

    protected function statement_resultHandler(event:SQLEvent):void {
        var result:SQLResult = SQLStatement(event.target).getResult();
        var parsedResult:* = parseResult(result);
        for each (var callback:Function in subscribers) {
            callback(parsedResult);
        }
    }
}

}
