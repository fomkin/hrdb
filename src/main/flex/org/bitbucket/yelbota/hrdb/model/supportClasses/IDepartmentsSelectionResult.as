package org.bitbucket.yelbota.hrdb.model.supportClasses {

/**
 * Employees model
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IDepartmentsSelectionResult extends IQueryResult {

    /**
     * @param resultHandler function (result:Vector.<Department>)void { ... }
     * @param errorHandler function (error:String)void { ... }
     */
}

}
