package org.bitbucket.yelbota.hrdb.model {

import flash.data.SQLStatement;
import flash.filesystem.File;

import org.osflash.signals.ISignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface ISQLService {

    function get onConnect():ISignal;

    function get onDisconnect():ISignal;

    function get onStartProcessQuery():ISignal;

    function get onFinishProcessQuery():ISignal;

    function open(file:File):void;

    function createNew(file:File):void;

    function executeStatement(statement:SQLStatement):void;

    function executeQuery(query:String):void;
}
}
