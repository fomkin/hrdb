package org.bitbucket.yelbota.hrdb.model {

import flash.data.SQLStatement;

import org.bitbucket.yelbota.hrdb.model.errors.DepartmentIsNotEmptyError;
import org.bitbucket.yelbota.hrdb.model.supportClasses.BooleanQueryResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IDepartmentsSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IQueryResult;
import org.bitbucket.yelbota.hrdb.value.Department;

/**
 * Employees model
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SQLDepartmentsModel extends SQLModelBase implements IDepartmentsModel {

    //-------------------------------------------------------------------------
    //
    //  IDepartmentsModel
    //
    //-------------------------------------------------------------------------

    public function getAllDepartmentsWithNumEmployees():IDepartmentsSelectionResult {
        var statement:SQLStatement = new SQLStatement();
        var result:DepartmentsSelectionResult = new DepartmentsSelectionResult(statement);
        statement.text = "SELECT *,(SELECT COUNT(*) FROM main.Employees WHERE DeptId = Departments.DeptId) as NumEmployees FROM Departments;";
        sqlService.executeStatement(statement);
        return result;
    }

    public function getAllDepartments():IDepartmentsSelectionResult {
        var statement:SQLStatement = new SQLStatement();
        var result:DepartmentsSelectionResult = new DepartmentsSelectionResult(statement);
        statement.text = "SELECT * FROM Departments;";
        sqlService.executeStatement(statement);
        return result;
    }

    public function createDepartment(value:Department):IQueryResult {
        var result:IQueryResult = executeQueryWithBasicResult("INSERT INTO Departments (DeptName) VALUES ('" + value.name + "');");
        result.subscribe(dispatchUpdate);
        return result;
    }

    public function removeDepartment(value:Department):IQueryResult {

        if (value.id == null) {
            throw new ArgumentError("Department id must be not null");
        }

        var statement:SQLStatement = new SQLStatement();
        var result:BooleanQueryResult = new BooleanQueryResult(statement);

        getAllDepartmentsWithNumEmployees().subscribe(
                function(departments:Vector.<Department>):void {
                    for each (var department:Department in departments) {
                        if (department.id == value.id) {
                            if (department.employeesNumber > 0) {
                                result.fail(new DepartmentIsNotEmptyError());
                            }
                            else {
                                statement.text = "DELETE FROM Departments WHERE DeptId = " + value.id + ";";
                                sqlService.executeStatement(statement);
                            }
                            break;
                        }
                    }
                },
                function(error:Error):void {
                    result.fail(error);
                }
        );

        result.subscribe(dispatchUpdate);
        return result;
    }
}

}

import flash.data.SQLResult;
import flash.data.SQLStatement;

import org.bitbucket.yelbota.hrdb.model.supportClasses.IDepartmentsSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.QueryResultBase;
import org.bitbucket.yelbota.hrdb.value.Department;

class DepartmentsSelectionResult extends QueryResultBase implements IDepartmentsSelectionResult {

    public function DepartmentsSelectionResult(statement:SQLStatement) {
        super(statement);
    }

    override protected function parseResult(result:SQLResult):* {
        var parsedResult:Vector.<Department> = new Vector.<Department>();
        for each (var row:Object in result.data) {
            var department:Department = new Department(row["DeptName"], row["NumEmployees"], row["DeptID"]);
            parsedResult.push(department);
        }
        return parsedResult;
    }
}