package org.bitbucket.yelbota.hrdb.model {

import org.bitbucket.yelbota.hrdb.model.supportClasses.IDepartmentsSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IQueryResult;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.osflash.signals.ISignal;

/**
 * Departments dictionary model
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IDepartmentsModel {

    function get onUpdate():ISignal;

    function getAllDepartmentsWithNumEmployees():IDepartmentsSelectionResult;

    function getAllDepartments():IDepartmentsSelectionResult;

    function createDepartment(value:Department):IQueryResult;

    /**
     * Handle DepartmentIsNotEmptyError in result
     */
    function removeDepartment(value:Department):IQueryResult;
}

}
