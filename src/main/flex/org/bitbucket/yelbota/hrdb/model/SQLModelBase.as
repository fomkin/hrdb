package org.bitbucket.yelbota.hrdb.model {

import flash.data.SQLStatement;
import flash.events.EventDispatcher;

import org.bitbucket.yelbota.hrdb.model.supportClasses.BooleanQueryResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IQueryResult;
import org.osflash.signals.ISignal;
import org.osflash.signals.Signal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
internal class SQLModelBase extends EventDispatcher {

    //-------------------------------------------------------------------------
    //
    //  Signals
    //
    //-------------------------------------------------------------------------

    protected var _onUpdate:Signal = new Signal();

    public function get onUpdate():ISignal {
        return _onUpdate;
    }

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var sqlService:ISQLService;

    [PostConstruct]
    public function subscribeSqlServiceEvents():void {
        sqlService.onConnect.add(_onUpdate.dispatch);
    }

    //-------------------------------------------------------------------------
    //
    //  Protected methods
    //
    //-------------------------------------------------------------------------

    protected function executeQueryWithBasicResult(text:String, params:Object = null):IQueryResult {

        var statement:SQLStatement = new SQLStatement();
        var result:IQueryResult = new BooleanQueryResult(statement);

        statement.text = text;

        if (params) {
            for (var p:String in params) {
                statement.parameters[p] = params[p];
            }
        }

        sqlService.executeStatement(statement);

        return result;
    }

    protected function dispatchUpdate(ok:Boolean):void {
        _onUpdate.dispatch();
    }
}

}
