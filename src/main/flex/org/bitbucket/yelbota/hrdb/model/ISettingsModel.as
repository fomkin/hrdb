package org.bitbucket.yelbota.hrdb.model {

import flash.filesystem.File;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface ISettingsModel {

    function get database():File;

    function set database(value:File):void;
}

}
