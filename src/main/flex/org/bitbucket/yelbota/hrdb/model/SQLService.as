package org.bitbucket.yelbota.hrdb.model {

import flash.data.SQLConnection;
import flash.data.SQLMode;
import flash.data.SQLStatement;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import org.osflash.signals.ISignal;
import org.osflash.signals.Signal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SQLService implements ISQLService {

    //-------------------------------------------------------------------------
    //
    //  Signals
    //
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    //  onConnect
    //-------------------------------------------------------------------------

    private var _onConnect:Signal;

    public function get onConnect():ISignal {
        return _onConnect;
    }

    //-------------------------------------------------------------------------
    //  onDisconnect
    //-------------------------------------------------------------------------

    private var _onDisconnect:Signal;

    public function get onDisconnect():ISignal {
        return _onDisconnect;
    }

    //-------------------------------------------------------------------------
    //  onStatProcessQuery
    //-------------------------------------------------------------------------

    private var _onStartProcessQuery:Signal;

    public function get onStartProcessQuery():ISignal {
        return _onStartProcessQuery;
    }

    //-------------------------------------------------------------------------
    //  onFinishProcessQuery
    //-------------------------------------------------------------------------

    private var _onFinishProcessQuery:Signal;

    public function get onFinishProcessQuery():ISignal {
        return _onFinishProcessQuery;
    }

    //-------------------------------------------------------------------------
    //
    //  Variables
    //
    //-------------------------------------------------------------------------

    private var connection:SQLConnection;

    private var queue:Vector.<SQLStatement>;

    private var busy:Boolean;

    private var currentStatement:SQLStatement;

    //-------------------------------------------------------------------------
    //
    //  Public methods
    //
    //-------------------------------------------------------------------------

    public function SQLService() {
        connection = new SQLConnection();
        queue = new Vector.<SQLStatement>();

        _onConnect = new Signal();
        _onDisconnect = new Signal();
        _onStartProcessQuery = new Signal();
        _onFinishProcessQuery = new Signal();
    }

    public function open(file:File):void {
        if (connection.connected) {
            connection.close();
            connection = new SQLConnection();
            open(file);
        }
        else {
            var openHandler:Function = function (e:SQLEvent):void {
                _onConnect.dispatch();
            }

            var errorHandler:Function = function (e:SQLErrorEvent):void {
                connection.removeEventListener(SQLErrorEvent.ERROR, errorHandler);
                connection.removeEventListener(SQLEvent.OPEN, openHandler);
                createNew(file);
            }

            connection.addEventListener(SQLErrorEvent.ERROR, errorHandler);
            connection.addEventListener(SQLEvent.OPEN, openHandler);
            connection.openAsync(file, SQLMode.UPDATE);
        }
    }

    public function createNew(file:File):void {

        connection.addEventListener(SQLEvent.OPEN, function (e:SQLEvent):void {
            _onConnect.dispatch();
        });

        File.applicationDirectory.resolvePath("empty.db").copyTo(file);
        connection.openAsync(file, SQLMode.CREATE);
    }

    public function executeStatement(statement:SQLStatement):void {
        if (busy) {
            queue.push(statement);
        }
        else {
            busy = true;
            currentStatement = statement;
            _onStartProcessQuery.dispatch();

            statement.sqlConnection = connection;
            statement.addEventListener(SQLEvent.RESULT, statement_resultHandler);
            statement.addEventListener(SQLErrorEvent.ERROR, statement_errorHandler);
            statement.execute();

        }
    }

    public function executeQuery(query:String):void {
        var statement:SQLStatement = new SQLStatement();
        statement.text = query;
        executeStatement(statement);
    }

    //-------------------------------------------------------------------------
    //
    //  Private methods
    //
    //-------------------------------------------------------------------------

    private function executeNext():void {
        busy = false;
        currentStatement = null;
        if (queue.length > 0) {
            executeStatement(queue.shift());
        }
    }

    //-------------------------------------------------------------------------
    //
    //  Event listeners
    //
    //-------------------------------------------------------------------------

    private function statement_resultHandler(event:SQLEvent):void {
        _onFinishProcessQuery.dispatch();
        executeNext();
    }

    private function statement_errorHandler(event:SQLErrorEvent):void {
        _onFinishProcessQuery.dispatch();
        executeNext();
    }
}

}
