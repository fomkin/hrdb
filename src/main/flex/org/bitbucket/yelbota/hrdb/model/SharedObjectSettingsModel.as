package org.bitbucket.yelbota.hrdb.model {
import flash.filesystem.File;
import flash.net.SharedObject;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SharedObjectSettingsModel implements ISettingsModel {

    private var sharedObject:SharedObject = SharedObject.getLocal("settings");

    public function get database():File {
        if (sharedObject.data["database"]) {
            return File.userDirectory.resolvePath(sharedObject.data.database);
        }
        else {
            return null;
        }
    }

    public function set database(value:File):void {
        sharedObject.data["database"] = value.nativePath;
    }
}

}
