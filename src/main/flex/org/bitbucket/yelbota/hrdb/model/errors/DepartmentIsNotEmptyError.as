package org.bitbucket.yelbota.hrdb.model.errors {

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class DepartmentIsNotEmptyError extends Error {
}

}
