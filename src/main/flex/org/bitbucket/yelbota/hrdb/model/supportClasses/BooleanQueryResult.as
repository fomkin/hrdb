package org.bitbucket.yelbota.hrdb.model.supportClasses {
import flash.data.SQLResult;
import flash.data.SQLStatement;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class BooleanQueryResult extends QueryResultBase implements IQueryResult {

    public function BooleanQueryResult(statement:SQLStatement) {
        super(statement);
    }

    override protected function parseResult(result:SQLResult):* {
        return true;
    }
}

}
