package org.bitbucket.yelbota.hrdb.model {

import org.bitbucket.yelbota.hrdb.model.supportClasses.EmployeeSelectionCriteria;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IEmployeeSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IQueryResult;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.bitbucket.yelbota.hrdb.value.Employee;
import org.osflash.signals.ISignal;

/**
 * Employees model
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IEmployeesModel {

    function get onUpdate():ISignal;

    function selectEmployees(criteria:Vector.<EmployeeSelectionCriteria>):IEmployeeSelectionResult;

    function fireEmployeeByCriteria(criteria:Vector.<EmployeeSelectionCriteria>):IQueryResult;

    function takeEmployee(value:Employee):IQueryResult;

    function transferEmployeeByCriteria(criteria:Vector.<EmployeeSelectionCriteria>, department:Department):IQueryResult;
}

}
