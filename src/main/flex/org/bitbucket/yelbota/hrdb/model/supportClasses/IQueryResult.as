package org.bitbucket.yelbota.hrdb.model.supportClasses {

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public interface IQueryResult {

    /**
     * @param resultHandler function (result:Boolean)void { ... }
     * @param errorHandler function (error:String)void { ... }
     */
    function subscribe(resultHandler:Function, errorHandler:Function = null):void;
}

}
