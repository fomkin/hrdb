package org.bitbucket.yelbota.hrdb.model {

import flash.data.SQLStatement;

import org.bitbucket.yelbota.hrdb.model.supportClasses.BooleanQueryResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.EmployeeSelectionCriteria;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IEmployeeSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IQueryResult;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.bitbucket.yelbota.hrdb.value.Employee;

/**
 * Employees model
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SQLEmployeesModel extends SQLModelBase implements IEmployeesModel {

    //-------------------------------------------------------------------------
    //
    //  Injections
    //
    //-------------------------------------------------------------------------

    [Inject]
    public var departmentsModel:IDepartmentsModel;

    //-------------------------------------------------------------------------
    //
    //  IEmployeesModel
    //
    //-------------------------------------------------------------------------

    public function selectEmployees(criteria:Vector.<EmployeeSelectionCriteria>):IEmployeeSelectionResult {
        var departments:Object = {};
        var statement:SQLStatement = new SQLStatement();
        var result:EmployeesSelectionResult = new EmployeesSelectionResult(statement, departments);
        departmentsModel.getAllDepartments().subscribe(function(result:Vector.<Department>):void {
            for each (var department:Department in result) {
                departments[department.id] = department;
            }
            var wherePart:String = criteria.length > 0 ? " WHERE " + criteriaToSQL(criteria) : "";
            statement.text = "SELECT * FROM Employees" + wherePart + ";";
            sqlService.executeStatement(statement);
        });
        return result;
    }

    public function fireEmployeeByCriteria(criteria:Vector.<EmployeeSelectionCriteria>):IQueryResult {
        if (!criteria || criteria.length == 0) {
            throw ArgumentError("At least one criteria must be specified");
        }
        var statement:SQLStatement = new SQLStatement();
        var result:BooleanQueryResult = new BooleanQueryResult(statement);
        statement.text = "DELETE FROM Employees WHERE " + criteriaToSQL(criteria) + ";";
        result.subscribe(dispatchUpdate);
        sqlService.executeStatement(statement);
        return result;
    }

    public function takeEmployee(value:Employee):IQueryResult {

        var params:Object = {
            "@DeptID": value.department.id,
            "@FirstName": value.firstName,
            "@LastName": value.lastName,
            "@Position": value.position
        };

        var result:IQueryResult = executeQueryWithBasicResult("INSERT INTO Employees "
                + "(DeptID, FirstName, LastName, Position) VALUES"
                + "(@DeptID, @FirstName, @LastName, @Position);", params);

        result.subscribe(dispatchUpdate);
        return result;
    }

    public function transferEmployeeByCriteria(criteria:Vector.<EmployeeSelectionCriteria>, department:Department):IQueryResult {
        var result:IQueryResult = executeQueryWithBasicResult("UPDATE Employees "
                + "SET DeptID = " + department.id + " "
                + "WHERE " + criteriaToSQL(criteria) + ";");

        result.subscribe(dispatchUpdate);
        return result;
    }

    //-------------------------------------------------------------------------
    //
    //  Private methods
    //
    //-------------------------------------------------------------------------

    private static function criteriaToSQL(criteria:Vector.<EmployeeSelectionCriteria>):String {

        var parts:Vector.<String> = new Vector.<String>();

        for each (var c:EmployeeSelectionCriteria in criteria) {
            switch (c.type) {
                case EmployeeSelectionCriteria.ID: {
                    parts.push("EmplID = " + c.value);
                    break;
                }
                case EmployeeSelectionCriteria.DEPARTMENT: {
                    parts.push("DeptID = " + Department(c.value).id);
                    break;
                }
                case EmployeeSelectionCriteria.FIRST_NAME: {
                    parts.push("FirstName = '" + c.value + "'");
                    break;
                }
                case EmployeeSelectionCriteria.LAST_NAME: {
                    parts.push("LastName = '" + c.value + "'");
                    break;
                }
                case EmployeeSelectionCriteria.POSITION: {
                    parts.push("Position = '" + c.value + "'");
                    break;
                }
                default: throw new Error("Invalid criteria");
            }
        }

        return parts.join(" AND ");
    }
}

}

import flash.data.SQLResult;
import flash.data.SQLStatement;

import org.bitbucket.yelbota.hrdb.model.supportClasses.IEmployeeSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.QueryResultBase;
import org.bitbucket.yelbota.hrdb.value.Employee;

class EmployeesSelectionResult extends QueryResultBase implements IEmployeeSelectionResult {

    private var departments:Object;

    function EmployeesSelectionResult(statement:SQLStatement, departments:Object) {
        super(statement);
        this.departments = departments;
    }

    override protected function parseResult(result:SQLResult):* {

        var parsedResult:Vector.<Employee> = new Vector.<Employee>();

        for each (var row:Object in result.data) {
            var employee:Employee = new Employee(
                    row["FirstName"],
                    row["LastName"],
                    row["Position"],
                    departments[row["DeptID"]],
                    row["EmplID"]
            );
            parsedResult.push(employee);
        }

        return parsedResult;
    }
}
