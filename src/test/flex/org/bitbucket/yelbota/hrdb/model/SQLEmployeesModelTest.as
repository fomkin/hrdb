package org.bitbucket.yelbota.hrdb.model {

import flash.events.Event;
import flash.events.EventDispatcher;

import org.bitbucket.yelbota.hrdb.model.mock.MockSQLiteService;
import org.bitbucket.yelbota.hrdb.model.stub.DepartmentsModelStub;
import org.bitbucket.yelbota.hrdb.model.supportClasses.EmployeeSelectionCriteria;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.bitbucket.yelbota.hrdb.value.Employee;
import org.flexunit.asserts.assertEquals;
import org.flexunit.asserts.fail;
import org.flexunit.async.Async;
import org.osflash.signals.utils.proceedOnSignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SQLEmployeesModelTest {

    private var model:IEmployeesModel;

    [Before(async)]
    public function before():void {
        var m:SQLEmployeesModel = new SQLEmployeesModel();
        var sqlService:MockSQLiteService = new MockSQLiteService();
        m.sqlService = sqlService;
        m.departmentsModel = new DepartmentsModelStub(new <Department>[
            new Department("d1"),
            new Department("d2"),
            new Department("d3")
        ]);
        model = m;
        proceedOnSignal(this, sqlService.onConnect);
    }

    [Test(async)]
    public function testSelectEmployees1():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        model.selectEmployees(new Vector.<EmployeeSelectionCriteria>()).subscribe(function(result:Vector.<Employee>):void {
            assertEquals(3, result.length);
            assertEquals("Doe", result[0].lastName);
            asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
        });

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testSelectEmployees2():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
                new EmployeeSelectionCriteria(EmployeeSelectionCriteria.LAST_NAME, "Doe")
        ];

        model.selectEmployees(criteria).subscribe(function(result:Vector.<Employee>):void {
            assertEquals(2, result.length);
            asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
        });

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testSelectEmployees3():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
            new EmployeeSelectionCriteria(EmployeeSelectionCriteria.DEPARTMENT, new Department("d2", 0, "2"))
        ];

        model.selectEmployees(criteria).subscribe(function(result:Vector.<Employee>):void {
            assertEquals(1, result.length);
            asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
        });

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testFireEmployeeByCriteria():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
            new EmployeeSelectionCriteria(EmployeeSelectionCriteria.DEPARTMENT, new Department("d2", 0, "2"))
        ];

        model.fireEmployeeByCriteria(criteria).subscribe(
            function(ok:Boolean):void {
                model.selectEmployees(criteria).subscribe(
                    function(result:Vector.<Employee>):void {
                        assertEquals(0, result.length);
                        asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                    },
                    function(error:Error):void {
                        fail(error.message);
                        asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                    }
                );
            },
            function(error:Error):void {
                fail(error.message);
                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
            }
        );

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testTakeEmployee():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();

        model.takeEmployee(new Employee("Ivan", "Ivanov", "Programmer", new Department("d2", 0, "2"))).subscribe(
                function(ok:Boolean):void {
                    model.selectEmployees(new <EmployeeSelectionCriteria>[new EmployeeSelectionCriteria(EmployeeSelectionCriteria.FIRST_NAME, "Ivan")]).subscribe(
                            function(result:Vector.<Employee>):void {
                                assertEquals(1, result.length);
                                assertEquals("Ivanov", result[0].lastName);
                                assertEquals("d2", result[0].department.name);
                                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                            },
                            function(error:Error):void {
                                fail(error.message);
                                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                            }
                    );
                },
                function(error:Error):void {
                    fail(error.message);
                    asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                }
        );

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testTransferEmployeeByCriteria():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
            new EmployeeSelectionCriteria(EmployeeSelectionCriteria.DEPARTMENT, new Department("d1", 0, "1"))
        ];

        model.transferEmployeeByCriteria(criteria, new Department("d3", 0, "3")).subscribe(
                function(ok:Boolean):void {
                    var criteria:Vector.<EmployeeSelectionCriteria> = new <EmployeeSelectionCriteria>[
                        new EmployeeSelectionCriteria(EmployeeSelectionCriteria.DEPARTMENT, new Department("d3", 0, "3"))
                    ];
                    model.selectEmployees(criteria).subscribe(
                            function(result:Vector.<Employee>):void {
                                assertEquals(2, result.length)
                                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                            },
                            function(error:Error):void {
                                fail(error.message);
                                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                            }
                    );
                },
                function(error:Error):void {
                    fail(error.message);
                    asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                }
        );

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

}

}
