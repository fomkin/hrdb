package org.bitbucket.yelbota.hrdb.model {

import flash.events.Event;
import flash.events.EventDispatcher;

import flashx.textLayout.debug.assert;

import org.bitbucket.yelbota.hrdb.model.errors.DepartmentIsNotEmptyError;
import org.bitbucket.yelbota.hrdb.model.mock.MockSQLiteService;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.flexunit.asserts.assertEquals;
import org.flexunit.asserts.fail;
import org.flexunit.async.Async;
import org.osflash.signals.utils.proceedOnSignal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class SQLDepartmentsModelTest {

    private var model:IDepartmentsModel;

    [Before(async)]
    public function before():void {
        var m:SQLDepartmentsModel = new SQLDepartmentsModel();
        var sqlService:MockSQLiteService = new MockSQLiteService();
        m.sqlService = sqlService;
        model = m;
        proceedOnSignal(this, sqlService.onConnect);
    }

    [Test(async)]
    public function testGetAllDepartments():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        model.getAllDepartmentsWithNumEmployees().subscribe(function(result:Vector.<Department>):void {
            assertEquals(3, result.length);
            assertEquals(2, result[0].employeesNumber);
            assertEquals("d2", result[1].name);
            asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
        });

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testCreateDepartment():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        model.createDepartment(new Department("d4")).subscribe(
                function(ok:Boolean):void {
                    model.getAllDepartmentsWithNumEmployees().subscribe(function(result:Vector.<Department>):void {
                        for each (var d:Department in result) {
                            if (d.name == "d4") {
                                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                                return;
                            }
                        }
                        fail("New department not found in selection");
                        asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                    });
                },
                function(error:Error):void {
                    fail(error.message);
                    asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                }
        );

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testRemoveDepartment():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        model.getAllDepartmentsWithNumEmployees().subscribe(function(result:Vector.<Department>):void {
            var len:uint = result.length;
            model.removeDepartment(new Department("d3", 0, "3"));
            model.getAllDepartmentsWithNumEmployees().subscribe(function(result:Vector.<Department>):void {
                if (result.length >= len) {
                    assert();
                }
                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
            });
        });

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    [Test(async)]
    public function testRemoveDepartment2():void {

        var asyncDispatcher:EventDispatcher = new EventDispatcher();
        model.removeDepartment(new Department("d2", 0, "2")).subscribe(
                function(ok:Boolean):void {
                    fail("Department is not empty but removed");
                    asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                },
                function(error:Error):void {
                    if (!(error is DepartmentIsNotEmptyError)) {
                        fail(error.message);
                    }
                    asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
                }
        );

        Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
    }

    /*
        private function testQueryResult(resultHandler:Function):Function {
            var asyncDispatcher:EventDispatcher = new EventDispatcher();
            Async.proceedOnEvent(this, asyncDispatcher, Event.COMPLETE);
            return function (...args):* {
                resultHandler(args);
                asyncDispatcher.dispatchEvent(new Event(Event.COMPLETE));
            }
        }
    */
}

}
