package org.bitbucket.yelbota.hrdb.model.mock {

import flash.filesystem.File;

import org.bitbucket.yelbota.hrdb.model.SQLService;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class MockSQLiteService extends SQLService {

    public function MockSQLiteService() {
        super();
        onConnect.add(makeTestDb);
        var dbName:String = "tmp" + (Math.random() * 0xFFFF).toString(0xF) + ".sqlite";
        open(File.applicationStorageDirectory.resolvePath(dbName));
    }

    private function makeTestDb():void {

        executeQuery("INSERT INTO \"Departments\" VALUES(1,'d1');");
        executeQuery("INSERT INTO \"Departments\" VALUES(2,'d2');");
        executeQuery("INSERT INTO \"Departments\" VALUES(3,'d3');");

        executeQuery("INSERT INTO \"Employees\" VALUES(1,1,'Jonh','Doe','Junk');");
        executeQuery("INSERT INTO \"Employees\" VALUES(2,1,'Jane','Doe','Junk');");
        executeQuery("INSERT INTO \"Employees\" VALUES(3,2,'Bilbo','Baggins','Explorer');");
    }
}
}
