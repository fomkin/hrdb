package org.bitbucket.yelbota.hrdb.model.stub {

import flash.events.EventDispatcher;

import org.bitbucket.yelbota.hrdb.model.IDepartmentsModel;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IDepartmentsSelectionResult;
import org.bitbucket.yelbota.hrdb.model.supportClasses.IQueryResult;
import org.bitbucket.yelbota.hrdb.value.Department;
import org.osflash.signals.ISignal;
import org.osflash.signals.Signal;

/**
 * @author Aleksey Fomkin (aleksey.fomkin@gmail.com)
 */
public class DepartmentsModelStub extends EventDispatcher implements IDepartmentsModel {

    private var values:Vector.<Department>;

    public function DepartmentsModelStub(values:Vector.<Department>) {
        this.values = values ? values : new Vector.<Department>();
        var i:int = 0;
        for each (var department:Department in this.values) {
            department.id = String(++i);
        }
    }

    public function getAllDepartmentsWithNumEmployees():IDepartmentsSelectionResult {
        return new DepartmentsSelectionResultStub(values);
    }

    public function getAllDepartments():IDepartmentsSelectionResult {
        return new DepartmentsSelectionResultStub(values);
    }

    public function createDepartment(value:Department):IQueryResult {
        var l:uint = parseInt(values[values.length].id);
        value.id = String(l+1);
        values.push(value);
        _onUpdate.dispatch();
        return new DepartmentsSelectionResultStub(values);
    }

    public function removeDepartment(value:Department):IQueryResult {
        for (var i:int = 0; i < values.length; i++) {
            if (values[i].id == value.id) {
                values.splice(i, 1);
                break;
            }
        }
        _onUpdate.dispatch();
        return new DepartmentsSelectionResultStub(values);
    }

    private var _onUpdate:Signal = new Signal();

    public function get onUpdate():ISignal {
        return _onUpdate;
    }
}

}

import org.bitbucket.yelbota.hrdb.model.supportClasses.IDepartmentsSelectionResult;
import org.bitbucket.yelbota.hrdb.value.Department;

class DepartmentsSelectionResultStub implements IDepartmentsSelectionResult {

    private var values:Vector.<Department>;

    function DepartmentsSelectionResultStub(values:Vector.<Department>) {
        this.values = values;
    }

    public function subscribe(resultHandler:Function, errorHandler:Function = null):void {
        resultHandler(values);
    }
}
